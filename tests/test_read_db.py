"""Test functions realted to reading the database to memory."""

import pytest
import subprocess
from subprocess import CompletedProcess
from unittest import mock

from mgedb.db import MGEdb, _get_top_level_dir


def test_project_top_level(monkeypatch):
    """Test that the project top level is read correctly."""
    # mock and replace subprocess
    mock_subprocess = mock.Mock(autospec=subprocess)
    mock_proc = mock.Mock(spec=CompletedProcess)
    mock_proc.stdout = b'/Some/path/mgedb\n'
    mock_subprocess.run.return_value = mock_proc
    monkeypatch.setattr('mgedb.db.subprocess', mock_subprocess)

    # test validation of top level dir
    with pytest.raises(FileNotFoundError):
        _get_top_level_dir()

    # test correct subprocess call
    mock_subprocess.run.assert_called_with(
        ['git', 'rev-parse', '--show-toplevel'],
        stdout=mock_subprocess.PIPE,
    )


def test_default_data_path():
    """Test that default database path."""
    db = MGEdb()
    # test default database path exists
    assert db.database_path.is_dir()

    # test default database sequence path exists
    assert db.database_path.joinpath('sequences.d').is_dir()


def test_default_fnames():
    """Test default file names corresponds to files in db."""
    db = MGEdb()
    db_files = {f.name for f in db.database_path.glob('*')}
    exp_db_fnames = {
        MGEdb.records_fname, MGEdb.references_fname, MGEdb.nomenclature_fname
    }

    # record file names of db files match default names in mgedb
    assert len(exp_db_fnames & db_files) == len(exp_db_fnames)

    # sequence file names of db files match default names in mgedb
    db_files = {
        f.name
        for f in db.database_path.joinpath('sequences.d').glob('*')
    }
    exp_db_fnames = {MGEdb.record_seq_fname, MGEdb.record_cds_fname}
    assert len(exp_db_fnames & db_files) == len(exp_db_fnames)
