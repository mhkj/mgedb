"""Test sequence manipulation."""

from mgedb.sequence import reverse_complement


def test_reverse_complement():
    """Test reverse complementation of sequences."""
    # test rev-comp string
    assert reverse_complement('atg') == 'cat'

    # test rev-comp byte
    assert reverse_complement(b'atg') == b'cat'

    # test rev-comp memoryview
    seq = memoryview(b'atg')
    assert reverse_complement(seq) == b'cat'
