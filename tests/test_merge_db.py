"""Test functions for merging databases."""
import cattr
import pytest
from mgedb.db import MGEdb, MgeRecordsType, ReferencesType
from mgedb.update import (SequencesType, merge_records, merge_references,
                          merge_sequences)


@pytest.fixture()
def new_db_fixture():
    """Fixture for new database."""
    new_db = 'tests/data/db_two'
    return MGEdb(new_db)


@pytest.fixture()
def current_db_fixture():
    """Fixture for current database."""
    current = 'tests/data/db_one'
    return MGEdb(current)


def test_simple_merge_refs(current_db_fixture, new_db_fixture):
    """Test merging references of two databases wo conflict."""
    raw_exp = {
        10366527: {
            "authors": ["Manwaring NP", "Skurray RA", "Firth N"],
            "date": "1999-05-14",
            "journal": "Plasmid",
            "pmid": 10366527,
            "title": "Nucleotide sequence of the F plasmid leading region.",
        },
        10477311: {
            "authors": ["Marais A", "Mendz GL", "Hazell SL", "Mégraud F"],
            "date":
            "1999-09-14",
            "journal":
            "Microbiol Mol Biol Rev",
            "pmid":
            10477311,
            "title":
            "Metabolism and genetics of Helicobacter pylori: the genome era.",
        },
        10489327: {
            "authors": ["Preston KE", "Radomski CC", "Venezia RA"],
            "date":
            "1999-09-14",
            "journal":
            "Plasmid",
            "pmid":
            10489327,
            "title":
            "The cassettes and 3' conserved segment of an integron from Klebsiella oxytoca plasmid pACM1.",
        },
    }
    expected = cattr.structure(raw_exp, ReferencesType)
    result = merge_references(current_db_fixture, new_db_fixture)
    assert result == expected


def test_simple_merge_seqs(current_db_fixture, new_db_fixture):
    """Test merging sequences of two databases wo conflict."""
    raw_exp = (
        {
            "title":
            "IS1D|1|NC_000913",
            "seq":
            "GGTGATGCTGCCAACTTACTGATTTAGTGTATGATGGTGTTTTTGAGGTTCTCCAGTGGCTTCTGTTTCTATCAGCTGTCCCTCCTGTTCAGCTACTGACGGGGTGGTGCGTAACGGCAAAAGCACTGCCGGACATCAGCGCTATCTCTGCTCTCACTGCCGTAAAACATGGCAACTGCAGTTCACTTACACCGCTTCTCAACCCGGTACGCACCAGAAAATCATTGATATGGCCATGAATGGCGTTGGATGCCGGGCAACAGCCCGCATTATGGGCGTTGGCCTCAACACGATTTTCCGCCATTTAAAAAACTCAGGCCGCAGTCGGTAACCTCGCGCATACAGCCGGGCAGTGACGTCATCGTCTGCGCGGAAATGGACGAACAGTGGGGATACGTCGGGGCTAAATCGCGCCAGCGCTGGCTGTTTTACGCGTATGACAGGCTCCGGAAGACGGTTGTTGCGCACGTATTCGGTGAACGCACTATGGCGACGCTGGGGCGTCTTATGAGCCTGCTGTCACCCTTTGACGTGGTGATATGGATGACGGATGGCTGGCCGCTGTATGAATCCCGCCTGAAGGGAAAGCTGCACGTAATCAGCAAGCGATATACGCAGCGAATTGAGCGGTATAACCTGAATCTGAGGCAGCACCTGGCACGGCTGGGACGGAAGTCGCTGTCGTTCTCAAAATCGGTGGAGCTGCATGACAAAGTCATCGGGCATTATCTGAACATAAAACACTATCAATAAGTTGGAGTCATTACC",
        },
        {
            "title":
            "IS3D|1|NC_000913",
            "seq":
            "TGATCTTACCCAGCAATAGTGGACACGCGGCTAAGTGAGTAAACTCTCAGTCAGAGGTGACTCACATGACAAAAACAGTATCAACCAGTAAAAAACCCCGTAAACAGCATTCGCCTGAATTTCGCAGTGAAGCCCTGAAGCTTGCTGAACGCATCGGTGTTACTGCCGCAGCCCGTGAACTCAGCCTGTATGAATCACAACTCTACAACTGGCGCAGTAAACAGCAAAATCAGCAGACGTCTTCTGAACGTGAACTGGAGATGTCTACCGAGATTGCACGTCTCAAACGCCAGCTGGCAGAACGGGATGAAGAGCTGGCTATCCTCCAAAAGGCCGCGACATACTTCGCGAAGCGCCTGAAATGAAGTATGTCTTTATTGAAAAACATCAGGCTGAGTTCAGCATCAAAGCAATGTGCCGCGTGCTCCGGGTGGCCCGCAGCGGCTGGTATACGTGGTGTCAGCGGCGGACAAGGATAAGCACGCGTCAGCAGTTCCGCCAACACTGCGACAGCGTTGTCCTCGCGGCTTTTACCCGGTCAAAACAGCGTTACGGTGCCCCACGCCTGACGGATGAACTGCGTGCTCAGGGTTACCCCTTTAACGTAAAAACCGTGGCGGCAAGCCTGCGCCGTCAGGGACTGAGGGCAAAGGCCTCCCGGAAGTTCAGCCCGGTCAGCTACCGCGCACACGGCCTGCCTGTGTCAGAAAATCTGTTGGAGCAGGATTTTTACGCCAGTGGCCCGAACCAGAAGTGGGCAGGAGACATCACGTACTTACGTACAGATGAAGGCTGGCTGTATCTGGCAGTGGTCATTGACCTGTGGTCACGTGCCGTTATTGGCTGGTCAATGTCGCCACGCATGACGGCGCAACTGGCCTGCGATGCCCTGCAGATGGCGCTGTGGCGGCGTAAGAGGCCCCGGAACGTTATCGTTCACACGGACCGTGGAGGCCAGTACTGTTCAGCAGATTATCAGGCGCAACTGAAGCGGCATAATCTGCGTGGAAGTATGAGCGCAAAAGGTTGCTGCTACGATAATGCCTGCGTGGAAAGCTTCTTTCATTCGCTGAAAGTGGAATGTATCCATGGAGAACACTTTATCAGCCGGGAAATAATGCGGGCAACGGTGTTTAATTATATCGAATGTGATTACAATCGGTGGCGGCGGCACAGTTGGTGTGGCGGCCTCAGTCCGGAACAATTTGAAAACAAGAACCTCGCTTAGGCCTGTGTCCATATTACGTGGGTAGGATCA",
        },
        {
            "title":
            "IS5U|1|NC_000913",
            "seq":
            "GGAAGGTGCGAACAAGTCCCTGATATGAGATCATGTTTGTCATCTGGAGCCATAGAACAGGGTTCATCATGAGTCATCAACTTACCTTCGCCGACAGTGAATTCAGCAGTAAGCGCCGTCAGACCAGAAAAGAGATTTTCTTGTCCCGCATGGAGCAGATTCTGCCATGGCAAAACATGGTGGAAGTCATCGAGCCGTTTTACCCCAAGGCTGGTAATGGCCGGCGACCTTATCCGCTGGAAACCATGCTACGCATTCACTGCATGCAGCATTGGTACAACCTGAGCGATGGCGCGATGGAAGATGCTCTGTACGAAATCGCCTCCATGCGTCTGTTTGCCCGGTTATCCCTGGATAGCGCCTTGCCGGACCGCACCACCATCATGAATTTCCGCCACCTGCTGGAGCAGCATCAACTGGCCCGCCAATTGTTCAAGACCATCAATCGCTGGCTGGCCGAAGCAGGCGTCATGATGACTCAAGGCACCTTGGTCGATGCCACCATCATTGAGGCACCCAGCTCGACCAAGAACAAAGAGCAGCAACGCGATCCGGAGATGCATCAGACCAAGAAAGGCAATCAGTGGCACTTTGGCATGAAGGCCCACATTGGTGTCGATGCCAAGAGTGGCCTGACCCACAGCCTGGTCACCACCGCGGCCAACGAGCATGACCTCAATCAGCTGGGTAATCTGCTGCATGGAGAGGAGCAATTTGTCTCAGCCGATGCCGGCTACCAAGGGGCGCCACAGCGCGAGGAGCTGGCCGAGGTGGATGTGGACTGGCTGATCGCCGAGCGCCCCGGCAAGGTAAGAACCTTGAAACAGCATCCACGCAAGAACAAAACGGCCATCAACATCGAATACATGAAAGCCAGCATCCGGGCCAGGGTGGAGCACCCATTTCGCATCATCAAGCGACAGTTCGGCTTCGTGAAAGCCAGATACAAGGGGTTGCTGAAAAACGATAACCAACTGGCGATGTTATTCACGCTGGCCAACCTGTTTCGGGCGGACCAAATGATACGTCAGTGGGAGAGATCTCACTAAAAACTGGGGATAACGCCTTAAATGGCGAAGAAACGGTCTAAATAGGCTGATTCAAGGCATTTACGGGAGAAAAAATCGGCTCAAACATGAAGAAATGAAATGACTGAGTCAGCCGAGAAGAATTTCCCCGCTTATTCGCACCTTCC",
        },
    )
    expected = cattr.structure(raw_exp, SequencesType)
    result = merge_sequences(current_db_fixture, new_db_fixture)
    assert result == expected


def simple_merge_records(current_db_fixture, new_db_fixture):
    """Test merging sequences of two databases wo conflict."""
    raw_exp = {
        'IS1D': {
            'name':
            'IS1D',
            'type':
            'is',
            'family':
            'IS1',
            'group':
            'None',
            'link': [{
                'name': 'isfinder',
                'db_id': 'IS1D'
            }],
            'synonyms': [],
            'evidence_lvl':
            'experimental',
            'organism': [{
                'name': 'Klebsiella pneumoniae',
                'taxid': '573'
            }],
            'references': ['16397293', '16738553', '9278503'],
            'sequences': [{
                'accession':
                'NC_000913',
                'partial':
                False,
                'cds':
                [{
                    'start':
                    1049832,
                    'end':
                    1050108,
                    'strand':
                    1,
                    'gene':
                    'insA-4',
                    'product':
                    'IS1 protein InsA',
                    'aa_seq':
                    'MASVSISCPSCSATDGVVRNGKSTAGHQRYLCSHCRKTWQLQFTYTASQPGTHQKIIDMAMNGVGCRATARIMGVGLNTIFRHLKNSGRSR'
                },
                 {
                     'start':
                     1050026,
                     'end':
                     1050530,
                     'strand':
                     1,
                     'gene':
                     'insB-4',
                     'product':
                     'IS1 protein InsB',
                     'aa_seq':
                     'MPGNSPHYGRWPQHDFPPFKKLRPQSVTSRIQPGSDVIVCAEMDEQWGYVGAKSRQRWLFYAYDRLRKTVVAHVFGERTMATLGRLMSLLSPFDVVIWMTDGWPLYESRLKGKLHVISKRYTQRIERYNLNLRQHLARLGRKSLSFSKSVELHDKVIGHYLNIKHYQ'
                 }],
                'start': [1049777],
                'end': [1050545],
                'irl':
                None,
                'irr':
                None
            }]
        },
        'IS3D': {
            'name':
            'IS3D',
            'type':
            'is',
            'family':
            'IS1',
            'group':
            'None',
            'link': [{
                'name': 'isfinder',
                'db_id': 'IS3D'
            }],
            'synonyms': [],
            'evidence_lvl':
            'experimental',
            'organism': [{
                'name': 'Klebsiella pneumoniae',
                'taxid': '573'
            }],
            'references': ['16397293', '16738553', '9278503'],
            'sequences': [{
                'accession':
                'NC_000913',
                'partial':
                False,
                'cds': [{
                    'start':
                    1094274,
                    'end':
                    1095141,
                    'strand':
                    -1,
                    'gene':
                    'insF-4',
                    'product':
                    'IS3 element protein InsF',
                    'aa_seq':
                    'MKYVFIEKHQAEFSIKAMCRVLRVARSGWYTWCQRRTRISTRQQFRQHCDSVVLAAFTRSKQRYGAPRLTDELRAQGYPFNVKTVAASLRRQGLRAKASRKFSPVSYRAHGLPVSENLLEQDFYASGPNQKWAGDITYLRTDEGWLYLAVVIDLWSRAVIGWSMSPRMTAQLACDALQMALWRRKRPRNVIVHTDRGGQYCSADYQAQLKRHNLRGSMSAKGCCYDNACVESFFHSLKVECIHGEHFISREIMRATVFNYIECDYNRWRRHSWCGGLSPEQFENKNLA'
                },
                        {
                            'start':
                            1095137,
                            'end':
                            1095437,
                            'strand':
                            -1,
                            'gene':
                            'insE-4',
                            'product':
                            'IS3 element protein InsE',
                            'aa_seq':
                            'MTKTVSTSKKPRKQHSPEFRSEALKLAERIGVTAAARELSLYESQLYNWRSKQQNQQTSSERELEMSTEIARLKRQLAERDEELAILQKAATYFAKRLK'
                        }],
                'start': [1094244],
                'end': [1095502],
                'irl':
                None,
                'irr':
                None
            }]
        },
        'IS5U': {
            'name':
            'IS5U',
            'type':
            'is',
            'family':
            'IS5',
            'group':
            'None',
            'link': [{
                'name': 'isfinder',
                'db_id': 'IS5U'
            }],
            'synonyms': [],
            'evidence_lvl':
            'experimental',
            'organism': [{
                'name': 'Klebsiella pneumoniae',
                'taxid': '573'
            }],
            'references': ['16397293', '16738553', '9278503'],
            'sequences': [{
                'accession':
                'NC_000913',
                'partial':
                False,
                'cds': [{
                    'start':
                    1299530,
                    'end':
                    1300547,
                    'strand':
                    1,
                    'gene':
                    'insH21',
                    'product':
                    'IS5 transposase and trans-activator',
                    'aa_seq':
                    'MFVIWSHRTGFIMSHQLTFADSEFSSKRRQTRKEIFLSRMEQILPWQNMVEVIEPFYPKAGNGRRPYPLETMLRIHCMQHWYNLSDGAMEDALYEIASMRLFARLSLDSALPDRTTIMNFRHLLEQHQLARQLFKTINRWLAEAGVMMTQGTLVDATIIEAPSSTKNKEQQRDPEMHQTKKGNQWHFGMKAHIGVDAKSGLTHSLVTTAANEHDLNQLGNLLHGEEQFVSADAGYQGAPQREELAEVDVDWLIAERPGKVRTLKQHPRKNKTAINIEYMKASIRARVEHPFRIIKRQFGFVKARYKGLLKNDNQLAMLFTLANLFRADQMIRQWERSH'
                }],
                'start': [1299498],
                'end': [1300693],
                'irl':
                None,
                'irr':
                None
            }]
        }
    }
    expected = cattr.structure(raw_exp, MgeRecordsType)
    result = merge_records(current_db_fixture, new_db_fixture)
    assert result == expected
