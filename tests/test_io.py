"""Test functions for reading and writing files."""

import json
from unittest import mock

import cattr
import pytest
from mgedb.io import SequencesType, write_db_file, write_fasta


def test_write_fasta_entries(tmpdir):
    """Test writing of fasta files.

    - test header
    - test sequence length
    """
    ENTRY = ({
        'title':
        'TRm21|NC_003078.1|434686|435854',
        'seq':
        'MSRFAACFEDLPDPRGRNARHPLTSILFIAVAAIVCGAESCTDMADFGVAKKKWLKTIVP',
    }, )
    entry = cattr.structure(ENTRY, SequencesType)

    # Test path
    p = tmpdir.join('test_write_fasta.fna')
    line_len = 10
    write_fasta(p, entry, line_len)

    with open(p) as i:
        raw_fa_file = [l.rstrip() for l in i]
        header = raw_fa_file[0]
        # Test header
        assert f'>{entry[0].title}' == header
        # Test chunking of sequence line length
        num_seq_lines = int(len(entry[0].seq) / line_len)
        assert len(raw_fa_file[1:]) == num_seq_lines
        assert all(line_len >= len(l) for l in raw_fa_file[1:])
        # Test that full lines has correct length
        if num_seq_lines > 1:
            assert all(line_len == len(l) for l in raw_fa_file[1:-1])


def test_write_db_file_json(tmpdir, monkeypatch):
    """Test writing database to file."""
    mock_cattr = mock.Mock(spec=cattr)
    monkeypatch.setattr('mgedb.io.cattr', mock_cattr)

    mock_json = mock.Mock(spec=json.dump, return_value='mock')
    monkeypatch.setattr('mgedb.io.json.dump', mock_json)

    write_db_file(tmpdir.join('test_write_db.json'), 'db')
    mock_cattr.unstructure.assert_called()
    mock_json.assert_called()

    # test overriding results
    write_db_file(tmpdir.join('test_write_db'), 'db', format='json')


def test_write_db_format_detection_error(tmpdir, monkeypatch):
    """Test writing database to file."""
    mock_cattr = mock.Mock(spec=cattr)
    monkeypatch.setattr('mgedb.io.cattr', mock_cattr)

    # Test unrecognized formats format
    with pytest.raises(ValueError):
        write_db_file(tmpdir.join('test_write_db.fomat'), 'db')

    # Test invalid format
    with pytest.raises(ValueError):
        write_db_file(tmpdir.join('test_write_db'), 'db', format='foo')
