# Instructions to release a new version of MGEdb

The instructions assume that the modules required for development specified in `requirements-devel.txt` has been installed.

1. Create a release branch with the release name, e.g. `release-1.1.1` and checkout the branch

    ```bash
    git checkout -b release-1.1.1
    ```

2. Update version with `bumpversion` and specify if its a major, minor or patch.

   `bumpversion [version type]`


3. Commit changes, push to github and create a pull request

    ```bash
    git add gens/__version__.py
    git add package.json CHANGELOG.md
    git commit -m "Release notes version 1.1.1"
    git push -u origin release-1.1.1
    ```

4. On bitbucket click **create pull request**.

5. After getting the pull request passes the automatic tests merge it to master.
