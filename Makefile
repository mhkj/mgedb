.PHONY: help list type test coverage

SHELL:=/usr/bin/env sh
VERSION = $(shell grep -oE "[0-9]+.[0-9]+.[0-9]+[a-z]*" mgedb/version.py)

.PHONY: help lint type clean test coverage bump-version

help:  ## Show this message
	@awk '\
	BEGIN {FS = ":.*##"} \
	/^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } \
	/^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } \
	' $(MAKEFILE_LIST)

clean: ## Remove docker image
	@rm -r *.egg-info MGEdb-*.*.* dist build

lint:  ## Lint MGEdb
	tox


test:  ## Test MGEdb
	tox

coverage:  ## Generate coverage report
	tox -e codecov

bump-version:  ## Tag the release
	@echo "Current version ${VERSION}"
	@read -p "Which should be increased (major.minor.patch): " target;		\
	if [[ $$target =~ (major|minor|patch)$$ ]]; then						\
	bump2version  $$target;													\
	else																	\
	echo $$target is not a valid option, use either major, minor or patch;	\
	fi
