#!/usr/bin/env python
"""Reformat database with to work with reduction."""
import yaml
try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper

filename = 'mge_records.yml'

with open(filename) as f:
    yml = yaml.load(f)

# change some values to be lists to help reducing the database
for k in yml:
    rec = yml[k]
    rec['accessions'] = [rec['accession']]
    del rec['accession']
    rec['organisms'] = [{'name': rec['organism'], 'taxid': rec['taxid']}]
    del rec['organism']
    del rec['taxid']
    yml[k] = rec

with open(filename, 'w') as o:
    o.write(yaml.dump(yml, Dumper=Dumper, default_flow_style=False))
