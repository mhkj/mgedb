#!/usr/bin/env python
"""Remove redundant sequences and update entries accordingly."""

from mgedb import MGEdb
from mgedb.io import write_fasta, write_db_file
from mgedb.db import MGErecord, Specie
from collections import defaultdict
from itertools import groupby
from pathlib import Path
import os


def _get_seq_accnr(seq_header):
    """Get accession number from seq header."""
    return seq_header.split('|')[1]


def _get_record_id(seq_header):
    """Get accession number from seq header."""
    return seq_header.split('|', 1)[1]


def _get_seq_id(record):
    return record.name + '|' + record.mge_id


def get_duplicate_seqs(db):
    """Find duplicate sequences in sequence object.

    Reported names are sorted on valid names first and secondly
    alpha numerically."""

    valid_names = {n for v in db.nomenclature.values() for n in v}

    entries = defaultdict(list)
    # groupy entries with identical sequences
    for seq_entry in db.record_sequences:
        entries[seq_entry.seq].append(seq_entry.title)

    for seq_titles in entries.values():
        if len(seq_titles) > 1:
            w_valid = {t for t in seq_titles if t.split('|')[0] in valid_names}
            wo_valid = set(seq_titles) - set(w_valid)
            yield sorted(w_valid) + sorted(wo_valid)


def _merge_entries(records, category, *accessions):
    """Merge records with ACCESSION in RECORDS on CATEGORY."""
    result = []
    for acc in accessions:
        value = getattr(records[acc], category)
        result += value if value is not None else []
    return result


if __name__ == '__main__':
    """Process
    1. read sequences to memory
    2. find duplicates
    for each duplicate
    - choice first sequence to keep, lowest sorted
    - if records have the same sequence and are from same source, if true
        - if they have same references, no action else update reference in kept record
        - if they have same type, no action else break
        - if other difference, break point
        - else remove redundant record
        - remove records
    - update mge_records with redundant sequences, identical ids
    - remove other sequences from seqs in memory
    """
    # Initialize database
    db = MGEdb()
    records = db.records
    record_seq = db.record_sequences

    tot_dupl_seqs = 0
    to_dupl_seqs = 0
    expected_removed_seqs = []
    before_reduction_cnt = len(records)
    removed = []
    for _, (keep, *remove) in enumerate(get_duplicate_seqs(db)):
        records_before = len(records)
        expected_removed_seqs += remove
        keep_id = _get_record_id(keep)
        to_merge = []
        # Reduce entries from same assembly
        for _, items in groupby([keep] + remove, key=_get_seq_accnr):
            items = list(items)  # Expand items
            if len(items) > 1:  # skip entries
                # join entries
                first, *rest = map(_get_record_id, items)
                all_rec_id = [first] + rest
                # Check for differences in pubmed idx
                if not all(records[first].pubmed_id == records[e].pubmed_id for e in rest):
                    import ipdb; ipdb.set_trace()
                if not all(records[first].type == records[e].type for e in rest):
                    import ipdb; ipdb.set_trace()
                # check that all cds has same gene and product name
                # this is a complication caused by cds position is absolute and not relative
                # Now they are assumed to be identical entreis and but one can be removed
                if keep_id in all_rec_id:
                    for i in set(all_rec_id) - set([keep_id]):
                        removed.append(i)
                        del records[i]
                else:
                    to_merge.append(first)
                    for i in rest:
                        removed.append(i)
                        del records[i]
            else:
                if keep != items[0]:
                    to_merge.append(_get_record_id(items[0]))

        # Merge entries and remove all merged entries
        r = records[keep_id]
        new_record = MGErecord(mge_id=r.mge_id,
                               name=r.name,
                               type=r.type,
                               accessions=_merge_entries(records, 'accessions', *to_merge),
                               pubmed_id=_merge_entries(records, 'pubmed_id', *to_merge),
                               organisms=_merge_entries(records, 'organisms', *to_merge),
                               nested_mges=r.nested_mges,
                               mge_feature=r.mge_feature,
                               irr=r.irr,
                               irl=r.irl,
                               cds=r.cds)
        records[keep_id] = new_record
        # Remove merged records from data structure
        for rec_id in to_merge:
            removed.append(rec_id)
            del records[rec_id]

        # sanity check that all records were removed
        try:
            assert records_before - len(records) == len(remove)
        except:
            import ipdb; ipdb.set_trace()

        # verify removal of records
        records_to_be_removed = map(_get_record_id, remove)
        assert all([r not in records for r in records_to_be_removed])
    # Sync records and sequences based on records
    record_idx = [_get_seq_id(r) for r in records.values()]
    seqs = {seq for seq in db.record_sequences() if seq.title in record_idx}
    total_seqs = len([s for s in db.record_sequences()])

    # Sanity check reduction
    assert before_reduction_cnt - len(records) == len(expected_removed_seqs)
    print(f'Removed: {before_reduction_cnt - len(records)} redundant sequences')

    # Write changes
    os.makedirs('reduced_database', exist_ok=True)
    new_record_path = Path('reduced_database/mge_records.yml').absolute()
    write_db_file(new_record_path, records)
    new_sequence_path = Path('./reduced_database/mge_records.fna').absolute()
    write_fasta(new_sequence_path, seqs)
