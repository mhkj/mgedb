#!/usr/bin/env python
"""Remove record entries in remove_entries.txt file."""

from pathlib import Path

from mgedb import MGEdb
from mgedb.io import write_db_file, write_fasta, parse_db_fasta_header

# read database
db = MGEdb()
sequences = {s.title: s for s in db.record_sequences}
records = db.records


with open('remove_entries.txt') as ipt:
    for cnt, seq_accnr in enumerate(ipt, start=1):
        raw_seq_header = seq_accnr.rstrip()
        header = parse_db_fasta_header(seq_accnr)
        # check if record could be removed
        if raw_seq_header in sequences:
            del sequences[raw_seq_header]
            del records[header['name']]
        else:
            print(f'Record could not be removed, entry "{raw_seq_header}" is not in sequences')


print(f'removed {cnt} samples')
# write sequences
records_path = Path('mge_records.json').absolute()
print(f'wrote records: {records_path}')
write_db_file(records_path, records, 'json')
sequence_path = Path('mge_records.fna')
print(f'wrote sequence: {sequence_path}')
sequences = [s for s in sequences.values()]
write_fasta(sequence_path, sequences)
